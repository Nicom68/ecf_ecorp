import pandas as pd
import pymysql
import mysql.connector


df = pd.read_csv("recettes.csv", sep=",") 


# Connexion à la base de données BDD_ECF
mydb = mysql.connector.connect(
    host="localhost",
    user="nicolas",
    password="DbxsvFb56K52A7;",
    database="BDD_ECF"
)

# df.info()
# les ingrédients en lower string
df['ingredient'] = df['ingredient'].str.lower()
# supprimer les guillemets
df['name'] = df['name'].str.replace('"', '')

cursor = mydb.cursor() 

# Insertion des noms de recette dans la table "recipe"
for index, row in df.iterrows():
    recipe_name = row['name'].strip()
cursor.execute("INSERT INTO recipe (recipe_name) VALUES (%s);", (recipe_name,))

# Vérifier si la recette existe déjà dans la table "Recipes"
cursor.execute("SELECT recipe_id FROM recipe WHERE recipe_name = %s", (row['name'],))
result = cursor.fetchone()

if result:
# Si la recette existe, utilisez l'ID existant
    recipe_id = result[0]
else:
# Si la recette n'existe pas, insérez-la dans la table "Recipes"
    cursor.execute("INSERT IGNORE INTO recipe (recipe_name) VALUES (%s)", (row['name'],))
# Valider les changements
mydb.commit()
# Récupérer l'ID de la recette insérée
recipe_id = cursor.lastrowid

# # Consume any unread result
cursor.fetchall()



