USE BDD_ECF;


CREATE TABLE IF NOT EXISTS recipe(
    recipe_id INT AUTO_INCREMENT PRIMARY KEY,
    recipe_name VARCHAR(255)
);

CREATE TABLE IF NOT EXISTS recipe_restaurant(
    recipe_id INT,
    product_id INT,
    qty INT,
    PRIMARY KEY (recipe_id, product_id),
    FOREIGN KEY (recipe_id) REFERENCES recipe(recipe_id),
    FOREIGN KEY (product_id) REFERENCES product(product_id)
    
);

    