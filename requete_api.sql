USE BDD_ECF;

-- En envoyant le nom d’un produit, connaître l’état des stocks dans
-- chaque restaurant ainsi que le stock global (cumulé).

SELECT product_name, SUM(qty) AS total_quantity
FROM product 
JOIN stock ON product.product_id = stock.product_id
GROUP BY product_name;
