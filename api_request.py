from fastapi import FastAPI
import mysql.connector

mydb = mysql.connector.connect(
    host="localhost",
    user="nicolas",
    password="DbxsvFb56K52A7;",
    database="BDD_SELL"
)

# uvicorn api_request:app --reload

app = FastAPI(debug=True)

# 1. Créer une route pour récupérer la liste des produits en json.
@app.get("/product")
async def create_product() -> list:
    mycursor = mydb.cursor(dictionary=True)
    sql = "SELECT product_name, SUM(qty) AS total_quantity FROM product JOIN stock ON product.product_id = stock.product_id GROUP BY product_name;"
    mycursor.execute(sql)
    stock_produit = mycursor.fetchall()

    return stock_produit

