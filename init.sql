DROP DATABASE IF EXISTS BDD_ECF;
CREATE DATABASE IF NOT EXISTS BDD_ECF;
USE BDD_ECF;


CREATE TABLE product(
    product_id INT PRIMARY KEY AUTO_INCREMENT,
    product_name VARCHAR(255)
);

CREATE TABLE restaurant(
    restaurant_id INT AUTO_INCREMENT PRIMARY KEY,
    restaurant_name VARCHAR(55)

);

CREATE TABLE stock(
    stock_id INT AUTO_INCREMENT PRIMARY KEY,
    restaurant_id INT,
    qty INT,
    product_id INT,
    FOREIGN KEY (product_id) REFERENCES product(product_id),
    FOREIGN KEY (restaurant_id) REFERENCES restaurant(restaurant_id)
);





