from fastapi import FastAPI
import mysql.connector

mydb = mysql.connector.connect(
    host="localhost",
    user="nicolas",
    password="DbxsvFb56K52A7;",
    database="BDD_ECF"
)

# uvicorn main:app --reload

app = FastAPI(debug=True)

# 1. Créer une route pour récupérer la liste des produits en json.
@app.get("/product/{product_name}")
async def stock_produit(product_name:str) -> list:
    mycursor = mydb.cursor(dictionary=True)
    sql = "SELECT product_name, SUM(qty) AS total_quantity FROM product JOIN stock ON product.product_id = stock.product_id GROUP BY product_name;"
    mycursor.execute(sql)
    stock_produit = mycursor.fetchall()
    return stock_produit


