import pandas as pd
import pymysql
import mysql.connector

# Connexion à la base de données BDD_ECF
mydb = mysql.connector.connect(
    host="localhost",
    user="nicolas",
    password="DbxsvFb56K52A7;",
    database="BDD_ECF"
)

df = pd.read_csv("restaurant_B.csv", sep=",")


# les produits en lower string
df['product_name'] = df['product_name'].str.lower()

# suppression du "s" de "oeufs"
df['product_name'] = df['product_name'].str.replace('oeufs', 'oeuf')

 # Regroupement par produit et par date en sélectionant la date la plus récente.Reset de l'index.
df_B = df.sort_values('date').groupby('product_name').last().reset_index()


cursor = mydb.cursor() 
cursor.execute("INSERT INTO restaurant VALUES (NULL, 'restaurant B');") 
restaurant_B_id = cursor.lastrowid 

 
mydb.commit()
for index, row in df_B.iterrows(): 
	print(row) 
	product = row['product_name'].replace("'", "\'") 
	print(product) 

	# Check if the product already exists
	cursor.execute("SELECT product_id FROM product WHERE product_name = %s", [product])
	existing_product = cursor.fetchone()

	if existing_product:
	# If the product already exists
		product_id = existing_product[0]
	else:
    # If the product does not exist, insert it into the 'product' table
		cursor.execute("INSERT INTO product (product_name) VALUES (%s);", [product])
		product_id = cursor.lastrowid

	# Ajouter à la table "stock"
	cursor.execute("INSERT INTO stock (restaurant_id, product_id, qty) VALUES (%s, %s, %s)", (restaurant_B_id, product_id, row['qty']))

# Valider les changements 
mydb.commit() 
# Fermer le curseur et la connexion 
cursor.close() 
mydb.close()
