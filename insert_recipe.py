import mysql.connector

# Connexion à la base de données BDD_ECF
mydb = mysql.connector.connect(
    host="localhost",
    user="nicolas",
    password="DbxsvFb56K52A7;",
    database="BDD_ECF"
)
cursor = mydb.cursor() 

    # Ajout des ingrédients manquants, recette Tiramisu
cursor.execute("INSERT INTO product (product_name) VALUES ('biscuit boudoir')")
cursor.execute("INSERT INTO product (product_name) VALUES ('cafe')")
cursor.execute("INSERT INTO product (product_name) VALUES ('rhum')")

    # Ajout de la recette "tiramisu" dans la table "recette"
cursor.execute("INSERT INTO recipe(recipe_name) VALUES ('tiramisu')")

    # Récupérer la recette et son id
cursor.execute("SELECT recipe_id FROM recipe WHERE recipe_name = 'tiramisu' LIMIT 1")
result = cursor.fetchone()
if result:
        recipe_id = result[0]

        # Ajout des ingrédients dans la table "recette"
        ingredients = [
            ('cafe', 100),
            ('oeuf', 3),
            ('sucre', 50),
            ('rhum', 100),
            ('biscuit boudoir', 30),
            ('cacao poudre', 5)
        ]

        for ingredient, qty in ingredients:
            cursor.execute("SELECT product_id FROM product WHERE product_name = %s LIMIT 1", (ingredient,))
            product_id_result = cursor.fetchone()

            if product_id_result:
                product_id = product_id_result[0]

                # Insertion dans la table"recipe_restaurant"
                cursor.execute("INSERT INTO recipe_restaurant (recipe_id, product_id, qty) VALUES (%s, %s, %s)",
                               (recipe_id, product_id, qty))

        # Commit 
        mydb.commit()
        print("recette insérée.")

else:
        print("Recette 'tiramisu' non trouvée.")


    # fermeture du cursor et de la connection
cursor.close()
mydb.close()