USE BDD_ECF;
-- Pour tester, créez dans la BDD des recettes avec des produits

-- ajout des ingrédients manquants pour faire les crèpes.
INSERT INTO product (product_name) VALUES ('sucre');
INSERT INTO product (product_name) VALUES ('lait');

-- insertion de la recette "crepe" dans la table "recipe"
INSERT INTO recipe(recipe_name) VALUES ('crepe');

-- récupérer la recette "crepe" et son id

SET @recipe_id := (SELECT recipe_id FROM recipe WHERE recipe_name = 'crepe' LIMIT 1);

-- ajout des ingrédients pour réaliser la recette "crepe" dans la table "product"
-- on utilise la table intermédiaire "recipe_restaurant"
-- on recherche les "id" des ingrédients (product_id) et on ajoute les quantités
INSERT INTO recipe_restaurant (recipe_id, product_id, qty) VALUES
(@recipe_id, (SELECT product_id FROM product WHERE product_name = 'farine' LIMIT 1), 100),
(@recipe_id, (SELECT product_id FROM product WHERE product_name = 'oeuf' LIMIT 1), 1),
(@recipe_id, (SELECT product_id FROM product WHERE product_name = 'sucre' LIMIT 1), 100),
(@recipe_id, (SELECT product_id FROM product WHERE product_name = 'lait' LIMIT 1), 100)
;





